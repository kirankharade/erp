using System;
using System.Windows.Data;

namespace SampleApp01.Converters
{
    public class EnumIntConverter : IValueConverter
    {
        public object ConvertBack(object value, Type targetType, object parameter,
            System.Globalization.CultureInfo culture)
        {
            return (Enum)Enum.Parse((Type)parameter, value.ToString());
        }

        public object Convert(object value, Type targetType, object parameter,
            System.Globalization.CultureInfo culture)
        {
            return (int)Enum.Parse((Type)parameter, value.ToString());
        }
        
    }
}

