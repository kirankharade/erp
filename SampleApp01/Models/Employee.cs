﻿using System.ComponentModel;
using MongoDB.Bson;

namespace SampleApp01.Models
{
   public enum SexType
   {
      Male,
      Female
   }


   public enum TitleType
   {
      Developer,
      Tester,
      Manager,
      President,
      [Description("Chairman of the Board")]
      Chairman,
      CEO
   }

   public class Employee
   {
      public ObjectId Id { get; set; }
      private string _name = string.Empty;
      private string _email = string.Empty;
      private SexType _sex = SexType.Male;
      private TitleType _title = TitleType.CEO;

      public string Name 
      { 
         get
         {
            return _name;
         }
         set
         {
            _name = value;
         }
      }

      public string Email 
      { 
         get
         {
            return _email;
         }
         set
         {
            _email = value;
         }
      }

      public SexType Sex 
      { 
         get
         {
            return _sex;
         }
         set
         {
            _sex = value;
         }
      }

      public TitleType Title 
      {
         get
         {
            return _title;
         }
         set
         {
            _title = value;
         }
      }

   }

}
