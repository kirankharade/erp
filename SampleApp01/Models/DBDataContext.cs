﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using MongoDB.Bson;
using MongoDB.Driver;

namespace SampleApp01.Models
{
    public class DbDataContext<T>
    {
        private MongoClient _client = null;
        private IMongoDatabase _db = null;
        private string _dbName = string.Empty;

        public DbDataContext(MongoClient client, string dbname)
        {
            this._client = client;
            this._dbName = dbname;
            this._db = _client.GetDatabase(this._dbName);
        }

        public IMongoDatabase Database
        {
            get { return _db; }
            set { _db = value; }
        }

        public IMongoCollection<T> Collection(string collectionName)
        {
            return _db.GetCollection<T>(collectionName);
        }

        public void Save(string collectionName, T item)
        {
            Collection(collectionName).InsertOne(item);
        }

        public void Save(string collectionName, IEnumerable<T> item)
        {
            Collection(collectionName).InsertMany(item);
        }

        public T Item(string collectionName, Func<T, int, bool> func)
        {
            var collection = Collection(collectionName);
            var found = collection.AsQueryable().Where(func);
            return found.AsQueryable().First();
        }

        public IEnumerable<T> AllItemsWith(string collectionName, Func<T, int, bool> func)
        {
            var collection = Collection(collectionName);
            return collection.AsQueryable().Where(func);
        }

        public IEnumerable<T> AllItems(string collectionName)
        {
            var collection = Collection(collectionName);
            return collection.AsQueryable().ToEnumerable();
        }


    }
}



