﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MongoDB.Bson;
using MongoDB.Driver;

namespace SampleApp01.Models
{

    public class DataAccessor
    {
        MongoClient _client = null;

        public DataAccessor()
        {
            _client = new MongoClient("mongodb://localhost:27017");
        }

        public MongoClient DbClient
        {
            get { return _client; }
            set { _client = value; }
        }

        public DbDataContext<T> CreateDataContext<T>(string dbname)
        {
            var ctx = new DbDataContext<T>(_client, dbname);
            return ctx;
        }


    }
}



