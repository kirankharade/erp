using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Reflection;
using System.Runtime.InteropServices.ComTypes;
using System.Windows.Data;
using System.Windows.Markup;

namespace SampleApp01.Utils
{
    public class TypeConverterUtils
    {

        public static string GetEnumDescription(Enum value)
        {
            FieldInfo fi = value.GetType().GetField(value.ToString());

            var a = (DescriptionAttribute[])fi.GetCustomAttributes(typeof(DescriptionAttribute), false);
            if (a != null && a.Length > 0)
                return a[0].Description;

            return value.ToString();
        }

        public static IEnumerable<T> EnumsToList<T>()
        {
            Type enumType = typeof(T);

            // Can't use generic type constraints on value types,
            // so have to do check like this
            if (enumType.BaseType != typeof(Enum))
                throw new ArgumentException("T must be of type System.Enum");

            Array enumValArray = Enum.GetValues(enumType);
            List<T> enumValList = new List<T>(enumValArray.Length);
            foreach (int val in enumValArray)
            {
                enumValList.Add((T)Enum.Parse(enumType, val.ToString()));
            }

            return enumValList;
        }

        public static ObservableCollection<String> EnumToStringList<T>()
        {
            Type enumType = typeof(T);

            // Can't use generic type constraints on value types,
            // so have to do check like this
            if (enumType.BaseType != typeof(Enum))
                throw new ArgumentException("T must be of type System.Enum");

            Array enumValArray = Enum.GetValues(enumType);
            List<String> enumValList = new List<String>(enumValArray.Length);
            foreach (int val in enumValArray)
            {
                var e = (Enum) Enum.Parse(enumType, val.ToString());
                enumValList.Add(GetEnumDescription(e));
            }
            return new ObservableCollection<String>(enumValList);
        }


    }



}