﻿using System.Windows.Controls;
using SampleApp01.ViewModels;

namespace SampleApp01.Views
{
    /// <summary>
    /// Interaction logic for Emplyee.xaml
    /// </summary>
    public partial class EmployeePage : UserControl
    {

        public EmployeePage()
        {
            InitializeComponent();
            this.DataContext = new EmployeeViewModel();
        }
    }
}


