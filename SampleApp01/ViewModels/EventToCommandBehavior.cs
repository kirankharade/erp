﻿namespace Renishaw.SPD.WiRE.CAPArray
{
    using System;
    using System.Reflection;
    using System.Windows;
    using System.Windows.Input;
    using System.Windows.Interactivity;

    /// <summary>
    /// This class can be used to define a behavior that execute command when given event is raised.
    /// </summary>
    public class EventToCommandBehavior : Behavior<FrameworkElement>
    {
        // Event
        public string Event 
        {
            get 
            { 
                return (string)GetValue(EventProperty); 
            } 

            set 
            { 
                this.SetValue(EventProperty, value); 
            }
        }

        public static readonly DependencyProperty EventProperty = DependencyProperty.Register("Event", typeof(string), typeof(EventToCommandBehavior), new PropertyMetadata(null, OnEventChanged));

        // Command
        public ICommand Command 
        { 
            get 
            { 
                return (ICommand)GetValue(CommandProperty); 
            }
 
            set 
            { 
                this.SetValue(CommandProperty, value); 
            }
        }

        public static readonly DependencyProperty CommandProperty = DependencyProperty.Register("Command", typeof(ICommand), typeof(EventToCommandBehavior), new PropertyMetadata(null));

        // PassArguments (default: false)
        public bool PassArguments 
        { 
            get 
            { 
                return (bool)GetValue(PassArgumentsProperty); 
            } 

            set 
            { 
                this.SetValue(PassArgumentsProperty, value); 
            }
        }

        public static readonly DependencyProperty PassArgumentsProperty = DependencyProperty.Register("PassArguments", typeof(bool), typeof(EventToCommandBehavior), new PropertyMetadata(false));

        private Delegate _Handler;
        private EventInfo _OldEvent;

        protected override void OnAttached()
        {
            this.AttachHandler(this.Event); // initial set
        }

        private static void OnEventChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var beh = (EventToCommandBehavior)d;

            // is not yet attached at initial load
            if (beh.AssociatedObject != null) 
            {
                beh.AttachHandler((string)e.NewValue);
            }
        }

        /// <summary>
        /// Attaches the handler to the event
        /// </summary>
        private void AttachHandler(string eventName)
        {
            // detach old event
            if (this._OldEvent != null)
            {
                this._OldEvent.RemoveEventHandler(this.AssociatedObject, this._Handler);
            }

            // attach new event
            if (!string.IsNullOrEmpty(eventName))
            {
                EventInfo ei = this.AssociatedObject.GetType().GetEvent(eventName);
                if (ei != null)
                {
                    MethodInfo mi = this.GetType().GetMethod("ExecuteCommand", BindingFlags.Instance | BindingFlags.NonPublic);
                    this._Handler = Delegate.CreateDelegate(ei.EventHandlerType, this, mi);
                    ei.AddEventHandler(this.AssociatedObject, this._Handler);
                    this._OldEvent = ei; // store to detach in case the Event property changes
                }
                else
                {
                    throw new ArgumentException(string.Format("The event '{0}' was not found on type '{1}'", eventName, this.AssociatedObject.GetType().Name));
                }
            }
        }

        /// <summary>
        /// Executes the Command
        /// </summary>
        private void ExecuteCommand(object sender, EventArgs e)
        {
            object parameter = this.PassArguments ? e : null;
            if (this.Command != null)
            {
                if (this.Command.CanExecute(parameter))
                {
                    this.Command.Execute(parameter);
                }
            }
        }
    }
}