﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using SampleApp01.Models;

namespace SampleApp01.ViewModels
{
    public class SaveEmployeeInfoCommand: RelayCommand
    {
        private readonly Employee _employee = null;
        private readonly DataAccessor _dataAccessor = null;

        public SaveEmployeeInfoCommand(ref Employee e, DataAccessor dataAccessor)
        {
            this._dataAccessor = dataAccessor;
            this._employee = e;
            _execute = PerformSave;
            _canExecute = CanPerformSave;
        }

        public bool CanPerformSave(object parameters)
        {
            return !string.IsNullOrEmpty(_employee.Name);
        }

        public void PerformSave(object parameters)
        {
            var empCopy = new Employee()
            {
                Name = _employee.Name,
                Email = _employee.Email,
                Sex = _employee.Sex,
                Title = _employee.Title
            };
            var dtx = _dataAccessor.CreateDataContext<Employee>("EmployeeDatabase");
            dtx.Save("Employees", empCopy);

            Console.WriteLine("Save Performed...");
        }
    }
}



