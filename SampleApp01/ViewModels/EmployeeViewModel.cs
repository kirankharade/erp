﻿using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Windows.Input;
using SampleApp01.Models;

namespace SampleApp01.ViewModels
{

    public class EmployeeViewModel: BaseViewModel
    {
        private Employee _employee = null;
        private ObservableCollection<string> _titles = new ObservableCollection<string>();
        private readonly DataAccessor _dataAccessor = new DataAccessor();

        public EmployeeViewModel()
        {
            this._employee = new Employee();
            SaveCommand = new SaveEmployeeInfoCommand(ref this._employee, _dataAccessor);
            Titles = Utils.TypeConverterUtils.EnumToStringList<Models.TitleType>();
        }
    
        public ObservableCollection<string> Titles 
        {
            get 
            {
                return _titles;
            } 
            set
            {
                _titles = value;
                OnPropertyChanged("Titles");
            }
        }

        public string EmployeeName
        {
            get { return _employee.Name; }

            set
            {
                _employee.Name = value;
                OnPropertyChanged();
            }
        }

        public string Email
        {
            get { return _employee.Email; }

            set
            {
                _employee.Email = value;
                OnPropertyChanged();
            }
        }

        public SexType Sex
        {
            get { return _employee.Sex; }

            set
            {
                _employee.Sex = value;
                OnPropertyChanged();
            }
        }

        public TitleType Title
        {
            get { return _employee.Title; }

            set
            {
                _employee.Title = value;
                OnPropertyChanged();
            }
        }

        public Employee ThisEmployee
        {
            get { return _employee; }

            set
            {
                _employee = value;
                OnPropertyChanged("ThisEmployee");
            }
        }

        public ICommand SaveCommand
        {
            get;
            internal set;
        }

    }
}

