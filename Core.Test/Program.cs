﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Common;
using System.Data.SqlClient;
using System.Data.EntityClient;
using Core.DataAccessLayer;
using Core.Models;
using Core.Models.Business;
using Core.Models.Employment;

namespace Core.Test
{
    class Program
    {
        private static void AddCompany()
        {
            var company = new Company
            {
                CompanyName = "Renishaw",
                RegistrationDate = DateTime.Now,
                Website = "www.Renishaw.com",
                PrimaryTrait = ECompanyType.Customer,
                SecondaryTrait = ECompanyType.Supplier,
                TertiaryTrait = ECompanyType.Partner
            };
            using (var context = new CoreDbContext())
            {
                context.Companies.Add(company);
                context.SaveChanges();
            }
        }

        static void Main(string[] args)
        {
            Database.SetInitializer(new DropCreateDatabaseIfModelChanges<CoreDbContext>()); 
            AddCompany();

            var cuow = new CoreUnitOfWork();
            var companyRepo = cuow.CompanyRepository;

            var companies = companyRepo.Get();
            foreach (var c in companies)
            {
                Console.WriteLine(c.ID);
                Console.WriteLine(c.CompanyName);
                Console.WriteLine(c.Website);
                Console.WriteLine(c.PrimaryTrait);
                Console.WriteLine(c.SecondaryTrait);
                Console.WriteLine(c.TertiaryTrait);
            }
        }
    }
}
