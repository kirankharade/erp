﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

using System.ComponentModel.DataAnnotations.Schema;
using Core.Models.Business;
using Core.Models.Employment;
using Core.Models.Operationals;

namespace Core.Models
{

    public class MyCompany : Organization
    {
        public List<Employee> Employees { get; set; }

        public Portfolio BusinessPortfolio { get; set; }

        public Finance Financials { get; set; }

        public Operations Ops { get; set; }

    }
}

