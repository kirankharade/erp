﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Linq.Expressions;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Common;
using System.Data.SqlClient;
using Core.Models;
using Core.DataAccessLayer;
using Core.Models.Business;
using Core.Models.Employment;

namespace Core.DataAccessLayer
{
    public class CoreUnitOfWork : IDisposable
    {
        private CoreDbContext m_coreContext = new CoreDbContext();
        private bool m_disposed = false;
        private CoreGenericRepository<Employee> m_employeeRepo = null;
        private CoreGenericRepository<Company> m_companyRepo = null;
        
        //private CoreGenericRepository<Organization> m_companyRepo = null;
        //private CoreGenericRepository<Organization> m_companyRepo = null;
        //private CoreGenericRepository<Organization> m_companyRepo = null;
        //private CoreGenericRepository<Organization> m_companyRepo = null;

        #region Repositories

        public CoreGenericRepository<Employee> EmployeeRepository
        {
            get
            {
                if (this.m_employeeRepo == null)
                {
                    this.m_employeeRepo = new CoreGenericRepository<Employee>(m_coreContext);
                }
                return m_employeeRepo;
            }
        }

        public CoreGenericRepository<Company> CompanyRepository
        {
            get
            {
                if (this.m_companyRepo == null)
                {
                    this.m_companyRepo = new CoreGenericRepository<Company>(m_coreContext);
                }
                return m_companyRepo;
            }
        }

            //public MyCompany GetMyCompany()
            //{
            //}

        #endregion

        public void Save()
        {
            m_coreContext.SaveChanges();
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!this.m_disposed)
            {
                if (disposing)
                {
                    m_coreContext.Dispose();
                }
            }
            this.m_disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }


    }
}

