﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Linq.Expressions;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Common;
using System.Data.SqlClient;
using Core.Models;
using Core.Models.Business;
using Core.Models.Employment;

namespace Core.DataAccessLayer
{
    public class CoreGenericRepository<T> where T : class
    {
        internal DbContext m_context;
        internal DbSet<T> m_dbSet = null;

        public CoreGenericRepository(DbContext context)
        {
            this.m_context = context;
            this.m_dbSet = context.Set<T>();
        }

        public virtual IEnumerable<T> Get(
            Expression<Func<T, bool>> filter = null,
            Func<IQueryable<T>, IOrderedQueryable<T>> orderBy = null,
            string includeProperties = "")
        {
            IQueryable<T> query = m_dbSet;

            if (filter != null)
            {
                query = query.Where(filter);
            }

            foreach (var includeProperty in includeProperties.Split
                (new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries))
            {
                query = query.Include(includeProperty);
            }

            if (orderBy != null)
            {
                return orderBy(query).ToList();
            }
            else
            {
                return query.ToList();
            }
        }

        public virtual T GetByID(object id)
        {
            return m_dbSet.Find(id);
        }

        public virtual void Insert(T entity)
        {
            m_dbSet.Add(entity);
        }

        public virtual void Delete(object id)
        {
            T entityToDelete = m_dbSet.Find(id);
            Delete(entityToDelete);
        }

        public virtual void Delete(T entityToDelete)
        {
            if (m_context.Entry(entityToDelete).State == EntityState.Detached)
            {
                m_dbSet.Attach(entityToDelete);
            }
            m_dbSet.Remove(entityToDelete);
        }

        public virtual void Update(T entityToUpdate)
        {
            m_dbSet.Attach(entityToUpdate);
            m_context.Entry(entityToUpdate).State = EntityState.Modified;
        }
    }
}
